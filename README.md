Developer: Cameron Blair
Developer contact: cameronblair@outlook.com
Purpose of site: To create a development(web and software) focused social media site with microposts, micropost feeds, and payments/free plans that give access to a users contact information
Tech used:
    Stripe API (for processing payments)
    Guard (for testing)
    

1. Run the project with the "Run Project" button in the menu bar on top of the IDE.
2. Preview your new app by clicking on the URL that appears in the Run panel below (https://connect-dev-blackheart32.c9users.io/).

Happy coding!
The Cloud9 IDE team


## Support & Documentation

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE. 
To watch some training videos, visit http://www.youtube.com/user/c9ide
