module ApplicationHelper
    
    #Returns the full title provided on the page or just ConnectDev if nothing is set
    def full_title(page_title = '')
        base_title = "ConnectDev"
        if page_title.empty?
            base_title
        else
            page_title + " | " + base_title
        end
    end
end
