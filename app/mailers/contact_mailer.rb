class ContactMailer < ActionMailer::Base
  default to: 'cameronblair@outlook.com'
  
  def contact_email(name, email, body)
    @name = name
    @email = email
    @body = body
    
    mail(from: email, subject: 'Message from a user at ConnectDev')
  end
end