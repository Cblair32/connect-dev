class InitialPagesController < ApplicationController
  def home
    
    if is_logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end
  
  
  def about
  end

end
