Rails.application.routes.draw do
  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'
  
  
  
  root 'initial_pages#home'
  get 'initial_pages/home'
  get '/about_site', to: 'initial_pages#site_about'
  get '/about_team', to: 'initial_pages#team_about'
  get '/signup',     to: 'users#new'
  get '/signup',     to: 'users#create'
  get '/login',      to: 'sessions#new'
  post '/login',     to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  resources :users do
      member do
        get :following, :followers
      end
    end
  resources :contacts
  resources :users
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  
end
