require 'test_helper'

class InitialPagesControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @base_title = "ConnectDev"
  end
  
  test "should get root" do
    get root_path
    assert_response :success
  end
  
  test "should get home" do
    get initial_pages_home_url
    assert_response :success
    assert_select "title", @base_title
  end

  test "should get contact" do
    get new_contact_path
    assert_response :success
    assert_select "title", "Contact | #{@base_title}"
  end


  test "should get site-about" do
    get about_site_path
    assert_response :success
    assert_select "title", "About Site | #{@base_title}"
  end
  
  test "should get team-about" do
    get about_team_path
    assert_response :success
    assert_select "title", "About Team | #{@base_title}"
  end
end
